/*
 * Copyright © 2019-2020 Rodney Dawes
 * Copyright © 2014 Robert Ancell <robert.ancell@gmail.com>
 *
 * This project is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This project is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import Ergo 0.0
import QtQuick 2.7
import QtQuick.Controls 2.2
import QtQuick.Window 2.0
import "games.js" as Games

ApplicationWindow {
    objectName: "mainView"
    id: game_view

    title: i18n.tr("Five Letters")
    visible: true

    minimumWidth: 360
    width: 400
    height: 640

    property var word_button_size: Math.min (width, height) / 6
    property var letter_button_size: Math.min (width, height) / 4

    property var letter_buttons: [ letter0, letter1, letter2, letter3, letter4 ]
    property var words: undefined
    property var solved_words: []
    property var given_up: false

    Gettext {
        domain: "five-letters"
    }

    Component.onCompleted: start_game ()

    Shortcut {
        context: Qt.ApplicationShortcut
        sequence: StandardKey.Quit
        onActivated: {
            Qt.quit();
        }
    }

    function start_game () {
        words = Games.get_random ()
        solved_words = []
        solved_words_model.clear ()
        given_up = false
        // TRANSLATORS: Text on button to give up
        give_up_button.text = i18n.tr ("Give Up")
        var letters = shuffle (words[0])
        word_button.clear ()
        for (var i = 0; i < letters.length; i++) {
            letter_buttons[i].text = letters[i].toUpperCase ()
            letter_buttons[i].letter = letters[i]
        }
        layout ()
    }

    function layout ()
    {
        var n_five = 0
        var n_four = 0
        var n_three = 0
        for (var i = 0; i < words.length; i++) {
            if (words[i].length == 3)
                n_three++
            else if (words[i].length == 4)
                n_four++
            else if (words[i].length == 5)
                n_five++
        }
        for (var i = 0; i < solved_words.length; i++) {
            if (solved_words[i].length == 3)
                n_three--
            else if (solved_words[i].length == 4)
                n_four--
            else if (solved_words[i].length == 5)
                n_five--
        }
        five_hint_label.text = i18n.tr ("Find <b>%n</b> five letter word", "Find <b>%n</b> five letter words", n_five).replace ('%n', n_five)
        five_hint_label.opacity = n_five > 0 ? 1.0 : 0.0
        four_hint_label.text = i18n.tr ("Find <b>%n</b> four letter word", "Find <b>%n</b> four letter words", n_four).replace ('%n', n_four)
        four_hint_label.opacity = n_four > 0 ? 1.0 : 0.0
        three_hint_label.text = i18n.tr ("Find <b>%n</b> three letter word", "Find <b>%n</b> three letter words", n_three).replace ('%n', n_three)
        three_hint_label.opacity = n_three > 0 ? 1.0 : 0.0
        if (n_five == 0 && n_four == 0 && n_three == 0) {
            // TRANSLATORS: Message shown when you solve the puzzle
            five_hint_label.text = i18n.tr ("Well done, you got all the words!")
            five_hint_label.opacity = 1.0
            // TRANSLATORS: Text on button to start new game
            give_up_button.text = i18n.tr ("New Game")
        }

        // Conditionally show hint labels
        if (n_five > 0) {
            four_hint_label.anchors.top = five_hint_label.bottom
            four_hint_label.anchors.topMargin = 12;
        }
        else {
            four_hint_label.anchors.top = letter4.bottom
            four_hint_label.anchors.topMargin = 24;
        }
        if (n_four > 0) {
            three_hint_label.anchors.top = four_hint_label.bottom
            three_hint_label.anchors.topMargin = 12
        }
        else if (n_five > 0) {
            three_hint_label.anchors.top = five_hint_label.bottom
            three_hint_label.anchors.topMargin = 12
        }
        else {
            three_hint_label.anchors.top = letter4.bottom
            three_hint_label.anchors.topMargin = 24
        }

        // Place word list below last hint
        if (n_three > 0)
            solved_words_flickable.anchors.top = three_hint_label.bottom
        else if (n_four > 0)
            solved_words_flickable.anchors.top = four_hint_label.bottom
        else
            solved_words_flickable.anchors.top = five_hint_label.bottom
    }

    function shuffle (word) {
        var letters = word.split ("")
        for (var i = letters.length - 1; i >= 0; i--)
        {
            var j = Math.floor (Math.random () * (i + 1))
            var t = letters[i]
            letters[i] = letters[j]
            letters[j] = t
        }
        return letters.join ("")
    }

    function add_letter (index) {
        if (letter_buttons[index].text == "")
            return

        word_button.add_letter (letter_buttons[index].letter)
        letter_buttons[index].text = ""

        layout ()
    }

    function give_up () {
        if (given_up || solved_words.length == words.length) {
            start_game ()
            return
        }

        given_up = true
        // TRANSLATORS: Text on button to start new game
        give_up_button.text = i18n.tr ("New Game")

        for (var i = words.length - 1; i >= 0; i--)
            if (!is_solved (words[i]))
                solved_words_model.insert (0, {"word": words[i], "is_solved": false})

        layout ()
    }

    function is_valid_word (word) {
        for (var i = 0; i < words.length; i++)
            if (words[i] == word)
                return true
        return false
    }

    function is_solved (word) {
        for (var i = 0; i < solved_words.length; i++)
            if (solved_words[i] == word)
                return true
        return false
    }

    function check_word () {
        var word = word_button.get_word ()
        if (is_valid_word (word)) {
            word_button.flash ("#34ed34")
            if (!is_solved (word)) {
                for (var i = solved_words.length; i > 0; i--)
                    solved_words[i] = solved_words[i - 1]
                solved_words[0] = word
                solved_words_model.insert (0, {"word": word, "is_solved": true})
                layout ()
            }
        }
        else
            word_button.flash ("#ed3434")

        word_button.clear ()
        for (var i = 0; i < letter_buttons.length; i++)
            letter_buttons[i].text = letter_buttons[i].letter.toUpperCase ()
    }

    Page {
        anchors.fill: parent
        Button {
            id: word_button
            height: word_button_size
            anchors.top: parent.top
            anchors.topMargin: 24
            anchors.left: parent.left
            anchors.leftMargin: 24
            anchors.right: parent.right
            anchors.rightMargin: 24
            font.pixelSize: word_button_size * 0.6
            onClicked: check_word ()
            property color orig_color
            property color flash_color
            Component.onCompleted: orig_color = color
            SequentialAnimation {
                id: flash_animation
                ColorAnimation {
                    target: word_button
                    to: word_button.flash_color
                }
                ColorAnimation {
                    target: word_button
                    to: word_button.orig_color
                }
            }
            Timer {
                id: hint_timer
                interval: 10000
                repeat: true
                onTriggered: word_button.flash ("white")
            }
            function flash (color) {
                flash_color = color
                flash_animation.start ()
            }
            function clear () {
                text = ""
                empty_label.opacity = 0.5
                hint_timer.stop ()
            }
            function add_letter (letter) {
                text += letter
                empty_label.opacity = 0
                if (text.length >= 3)
                    hint_timer.start ()
            }
            function get_word () {
                return text
            }
            Label {
                id: empty_label
                anchors.fill: parent
                // TRANSLATORS: Placeholder text for space to enter word
                text: i18n.tr ("Enter word")
                opacity: 0.5
                font.pixelSize: word_button_size * 0.6
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                Behavior on opacity {
                    NumberAnimation {
                    }
                }
            }
        }

        Button {
            id: letter0
            width: letter_button_size
            height: letter_button_size
            anchors.top: word_button.bottom
            anchors.topMargin: 24
            anchors.right: letter1.left
            anchors.rightMargin: 12
            font.bold: true
            font.pixelSize: letter_button_size * 0.6
            property var letter
            onClicked: game_view.add_letter (0)
        }
        Button {
            id: letter1
            width: letter_button_size
            height: letter_button_size
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.top: letter0.top
            font.bold: true
            font.pixelSize: letter_button_size * 0.6
            property var letter
            onClicked: game_view.add_letter (1)
        }
        Button {
            id: letter2
            width: letter_button_size
            height: letter_button_size
            anchors.top: letter0.top
            anchors.left: letter1.right
            anchors.leftMargin: 12
            font.bold: true
            font.pixelSize: letter_button_size * 0.6
            property var letter
            onClicked: game_view.add_letter (2)
        }
        Button {
            id: letter3
            width: letter_button_size
            height: letter_button_size
            anchors.top: letter1.bottom
            anchors.topMargin: 12
            anchors.right: letter1.horizontalCenter
            anchors.rightMargin: 12 * 0.5
            font.bold: true
            font.pixelSize: letter_button_size * 0.6
            property var letter
            onClicked: game_view.add_letter (3)
        }
        Button {
            id: letter4
            width: letter_button_size
            height: letter_button_size
            anchors.top: letter3.top
            anchors.left: letter1.horizontalCenter
            anchors.leftMargin: 12 * 0.5
            font.bold: true
            font.pixelSize: letter_button_size * 0.6
            property var letter
            onClicked: game_view.add_letter (4)
        }

        Label {
            id: five_hint_label
            anchors.top: letter4.bottom
            anchors.topMargin: 24
            anchors.horizontalCenter: parent.horizontalCenter
            textFormat: Text.StyledText
            horizontalAlignment: Text.AlignHCenter
            font.pixelSize: 18
            Behavior on opacity {
                NumberAnimation {
                }
            }
            Behavior on y {
                NumberAnimation {
                }
            }
        }
        Label {
            id: four_hint_label
            anchors.top: five_hint_label.bottom
            anchors.topMargin: 24
            anchors.horizontalCenter: parent.horizontalCenter
            textFormat: Text.StyledText
            horizontalAlignment: Text.AlignHCenter
            font.pixelSize: 18
            Behavior on opacity {
                NumberAnimation {
                }
            }
            Behavior on y {
                NumberAnimation {
                }
            }
        }
        Label {
            id: three_hint_label
            anchors.top: four_hint_label.bottom
            anchors.topMargin: 12
            anchors.horizontalCenter: parent.horizontalCenter
            textFormat: Text.StyledText
            horizontalAlignment: Text.AlignHCenter
            font.pixelSize: 18
            Behavior on opacity {
                NumberAnimation {
                }
            }
            Behavior on y {
                NumberAnimation {
                }
            }
        }

        ListModel {
            id: solved_words_model
        }

        Flickable {
            id: solved_words_flickable
            anchors.top: three_hint_label.bottom
            anchors.topMargin: 24
            anchors.bottom: give_up_button.top
            anchors.bottomMargin: 24
            anchors.left: parent.left
            anchors.right: parent.right
            contentWidth: parent.width
            contentHeight: solved_column.height
            clip: true
            Behavior on y {
                NumberAnimation {
                }
            }

            Column {
                id: solved_column
                Repeater {
                    model: solved_words_model
                    Label {
                        textFormat: Text.StyledText
                        width: solved_words_flickable.width
                        font.strikeout: is_solved
                        horizontalAlignment: Text.AlignHCenter
                        text: word
                        font.pixelSize: 16
                    }
                }
            }
        }

        Button {
            id: give_up_button
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 24
            onClicked: game_view.give_up ()
            font.pixelSize: 22
            width: 160
        }
    }
}
