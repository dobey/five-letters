# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2016-05-29 17:22+1200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"

#. TRANSLATORS: Text on button to give up
#: main.qml:39
msgid "Give Up"
msgstr ""

#: main.qml:70
msgid "Find <b>%n</b> five letter word"
msgid_plural "Find <b>%n</b> five letter words"
msgstr[0] ""
msgstr[1] ""

#: main.qml:72
msgid "Find <b>%n</b> four letter word"
msgid_plural "Find <b>%n</b> four letter words"
msgstr[0] ""
msgstr[1] ""

#: main.qml:74
msgid "Find <b>%n</b> three letter word"
msgid_plural "Find <b>%n</b> three letter words"
msgstr[0] ""
msgstr[1] ""

#. TRANSLATORS: Message shown when you solve the puzzle
#: main.qml:78
msgid "Well done, you got all the words!"
msgstr ""

#. TRANSLATORS: Text on button to start new game
#: main.qml:81 main.qml:150
msgid "New Game"
msgstr ""

#. TRANSLATORS: Placeholder text for space to enter word
#: main.qml:248
msgid "Enter word"
msgstr ""

#: five-letters.desktop.in.h:1
msgid "Five Letters"
msgstr ""

#: five-letters.desktop.in.h:2
msgid "Make words from five letters"
msgstr ""

#: five-letters.desktop.in.h:3
msgid "letters,puzzle,five,word,builder"
msgstr ""
